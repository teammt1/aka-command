package teammt.akacommand.aliases;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.command.PluginCommand;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import lombok.Getter;
import masecla.mlib.classes.Registerable;
import masecla.mlib.classes.Replaceable;
import masecla.mlib.main.MLib;
import teammt.akacommand.aliases.special.ChangeServerHandler;
import teammt.akacommand.aliases.special.FileMessageHandler;
import teammt.akacommand.aliases.special.MessageHandler;
import teammt.akacommand.aliases.special.SoundHandler;
import teammt.akacommand.aliases.special.SpecialHandler;
import teammt.akacommand.events.AliasCreateEvent;
import teammt.akacommand.events.AliasDeleteEvent;
import teammt.akacommand.usage.UsageManager;

public class AliasManager extends Registerable {

    private UsageManager usageManager;

    @Getter
    private Set<UUID> inTestMode = new HashSet<>();

    @Getter
    private Set<UUID> excluded = new HashSet<>();

    private Map<String, SpecialHandler> specialHandlers = new HashMap<>();

    public AliasManager(MLib lib, UsageManager usageManager) {
        super(lib);
        this.usageManager = usageManager;
    }

    @Override
    public void register() {
        super.register();

        this.loadSpecialHandlers();
        this.loadAliases();
    }

    private void loadSpecialHandlers() {
        new ChangeServerHandler(lib, this).register();
        new FileMessageHandler(lib, this).register();
        new MessageHandler(lib, this).register();
        new SoundHandler(lib, this).register();
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onCommandPre(PlayerCommandPreprocessEvent event) {
        String command = event.getMessage();
        for (CommandAlias alias : aliases.values()) {
            List<String> result = alias.matches(event);
            if (result != null) {
                if (inTestMode.contains(event.getPlayer().getUniqueId())) {
                    event.setCancelled(true);
                    lib.getMessagesAPI().sendMessage("command-test-mode-intercept", event.getPlayer(),
                            new Replaceable("%command%", result));
                    return;
                }

                result = lib.getPlaceholderAPI().applyOn(result, event.getPlayer());
                this.executeCommands(event.getPlayer(), result);

                event.setCancelled(true);
                return;
            }
        }

        String baseCommand = command.split(" ")[0].substring(1);
        PluginCommand pluginCommand = Bukkit.getServer().getPluginCommand(baseCommand);

        if (pluginCommand == null) {
            this.usageManager.ingestCommand(command);
            return;
        }
    }

    private void executeCommands(Player player, List<String> commands) {
        for (String command : commands) {
            command = command.replace("%player%", player.getName());
            command = lib.getPlaceholderAPI().applyOn(command, player);

            if (command.startsWith("/")) {
                player.performCommand(command.substring(1));
            } else if (command.startsWith("%")) {
                String special = command.split(" ")[0];
                SpecialHandler handler = specialHandlers.get(special);
                if (handler != null) {
                    handler.handle(player, command.substring(special.length() + 1));
                } else {
                    lib.getMessagesAPI().sendMessage("special-handler-not-found", player,
                            new Replaceable("%special%", special));
                }
            } else {
                player.chat(command);
            }
        }
    }

    public Set<String> getAliasNames() {
        return this.aliases.keySet();
    }

    @Getter
    private Map<String, CommandAlias> aliases = new HashMap<>();

    public void loadAliases() {
        aliases.clear();
        for (String alias : lib.getConfigurationAPI().getConfig().getConfigurationSection("aliases")
                .getKeys(false)) {
            ConfigurationSection section = lib.getConfigurationAPI().getConfig()
                    .getConfigurationSection("aliases." + alias);
            aliases.put(alias, new CommandAlias(section));
        }
    }

    public void createAlias(String id, CommandAlias alias) {
        aliases.put(id, alias);
        alias.save(lib.getConfigurationAPI().getConfig().createSection("aliases." + id));

        lib.getConfigurationAPI().updateFile("config");

        AliasCreateEvent aliasCreateEvent = new AliasCreateEvent(alias);
        Bukkit.getServer().getPluginManager().callEvent(aliasCreateEvent);
    }

    public void deleteAlias(String alias) {
        aliases.remove(alias);
        lib.getConfigurationAPI().getConfig().unset("aliases." + alias);

        AliasDeleteEvent event = new AliasDeleteEvent(alias);
        Bukkit.getServer().getPluginManager().callEvent(event);
    }

    public String getPossibleNameForAlias(String alias) {
        String name = alias;
        // Remove all non-alphanumeric characters
        name = name.replaceAll("[^a-zA-Z0-9]", "");
        // Remove all numbers from the start
        name = name.replaceAll("^[0-9]+", "");

        Set<String> alreadyTaken = getAliasNames();
        int i = 0;
        while (alreadyTaken.contains(name + i)) {
            i++;
        }

        if (i == 0)
            return name;
        return name + i;
    }

    public CommandAlias getAlias(String alias) {
        return aliases.get(alias);
    }

    public boolean aliasExists(String alias) {
        return aliases.containsKey(alias);
    }

    public void registerSpecialHandler(SpecialHandler specialHandler) {
        String prefix = specialHandler.getPrefix();
        if (specialHandlers.containsKey(prefix)) {
            lib.getLoggerAPI().warn("Special handler with prefix " + prefix + " already exists!", true);
            return;
        }

        specialHandlers.put(prefix, specialHandler);
    }
}
