package teammt.akacommand.aliases;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CommandAlias {
    private List<String> commands;
    private String alias;

    public CommandAlias(ConfigurationSection section) {
        if (section.isList("command")) {
            this.commands = section.getStringList("command");
        } else {
            this.commands = new ArrayList<>();
            this.commands.add(section.getString("command"));
        }

        this.alias = section.getString("alias");
    }

    public void save(ConfigurationSection section) {
        if (commands.size() == 1) {
            section.set("command", commands.get(0));
        } else {
            section.set("command", commands);
        }

        section.set("alias", alias);
    }

    public Pattern buildAliasPattern() {
        String[] aliasArgs = alias.split(" ");
        String regex = "";
        for (String arg : aliasArgs) {
            if (arg.startsWith("<") && arg.endsWith(">")) {
                regex += "(?" + arg + ".*?) ";
            } else {
                regex += Pattern.quote(arg) + " ";
            }
        }

        return Pattern.compile(regex.trim());
    }

    /**
     * This will check if the alias matches the executed command.
     * It will return the actual command to be executed if it matches,
     * with all the arguments replaced with the ones from the executed command.
     * 
     * @param event
     * @return
     */
    public List<String> matches(PlayerCommandPreprocessEvent event) {
        String executedCommand = event.getMessage();

        // The alias is in the <alias> <arg1> <arg2> <arg3> format
        String baseCommand = executedCommand.split(" ")[0];
        String baseAlias = alias.split(" ")[0];

        // First check the base command
        if (!baseCommand.equalsIgnoreCase(baseAlias))
            return null;

        // Extract all the arguments from the executed command
        Pattern pattern = buildAliasPattern();
        // Make sure the command matches the alias
        Matcher matcher = pattern.matcher(executedCommand);
        if (!matcher.matches())
            return null;

        // Obtain all the names of the arguments from the alias
        String[] aliasArgs = alias.split(" ");
        List<String> argNames = new ArrayList<>();

        for (String arg : aliasArgs) {
            if (arg.startsWith("<") && arg.endsWith(">")) {
                argNames.add(arg.substring(1, arg.length() - 1));
            }
        }

        // Extract all the arguments from the executed command
        Map<String, String> args = new HashMap<>();

        for (String argName : argNames)
            args.put(argName, matcher.group(argName));

        // Replace all the arguments in the command with the ones from the executed
        // command
        List<String> finalCommand = new ArrayList<>(commands);
        for (String argName : argNames) {
            for (int i = 0; i < finalCommand.size(); i++) {
                finalCommand.set(i, finalCommand.get(i).replace("<" + argName + ">", args.get(argName)));
            }
        }

        return finalCommand;
    }

    private String calculateRoot(String command) {
        String result = command.split(" ")[0];
        if (result.startsWith("/"))
            result = result.substring(1);
        return result;
    }

    public String getAliasRoot() {
        return calculateRoot(alias);
    }
}
