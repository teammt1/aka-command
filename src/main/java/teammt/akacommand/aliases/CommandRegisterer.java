package teammt.akacommand.aliases;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.command.CommandMap;
import org.bukkit.command.PluginCommand;
import org.bukkit.event.EventHandler;
import org.bukkit.plugin.Plugin;

import masecla.mlib.classes.Registerable;
import masecla.mlib.main.MLib;
import teammt.akacommand.events.AliasCreateEvent;
import teammt.akacommand.events.AliasDeleteEvent;
import teammt.akacommand.main.AKACommand;

public class CommandRegisterer extends Registerable {

    private AKACommand plugin;
    private AliasManager aliasManager;

    public CommandRegisterer(MLib lib, AKACommand plugin, AliasManager aliasManager) {
        super(lib);
        this.plugin = plugin;
        this.aliasManager = aliasManager;
    }

    @Override
    public void register() {
        super.register();
        updateAllAliases();
    }

    public void updateAllAliases() {
        CommandMap map = getCommandMap();

        aliasManager.getAliases().values().forEach(s -> {
            String root = s.getAliasRoot();
            PluginCommand command = createCommand(plugin, root);
            map.register(command.getPlugin().getName(), command);
        });

        syncCommands();
    }

    private PluginCommand createCommand(Plugin plugin, String cmd) {
        PluginCommand command = null;
        cmd = cmd.toLowerCase();

        try {
            Constructor<PluginCommand> constructor = PluginCommand.class.getDeclaredConstructor(
                    String.class, Plugin.class);
            constructor.setAccessible(true);
            command = constructor.newInstance(cmd, plugin);
        } catch (Exception e) {
            throw new RuntimeException("Could not create PluginCommand", e);
        }

        return command;
    }

    private CommandMap getCommandMap() {
        Server server = Bukkit.getServer();
        try {
            Method m = server.getClass().getDeclaredMethod("getCommandMap");
            m.setAccessible(true);
            return (CommandMap) m.invoke(Bukkit.getServer());
        } catch (Exception ignored) {
        }
        try {
            Field commandMapField = server.getClass().getDeclaredField("commandMap");
            commandMapField.setAccessible(true);
            return (CommandMap) commandMapField.get(server);
        } catch (Exception e) {
            throw new RuntimeException("Could not get commandMap", e);
        }
    }

    private void syncCommands() {
        try {
            Method syncCommandsMethod = Bukkit.getServer().getClass().getDeclaredMethod("syncCommands");
            syncCommandsMethod.setAccessible(true);
            syncCommandsMethod.invoke(Bukkit.getServer());
        } catch(ReflectiveOperationException e){
            lib.getLoggerAPI().warn("Could not find syncCommands method, tab completion might not work fully!");
        }
    }

    @EventHandler
    public void onAliasCreate(AliasCreateEvent event) {
        this.updateAllAliases();
    }

    @EventHandler
    public void onAliasDelete(AliasDeleteEvent event) {
        this.updateAllAliases();
    }
}
