package teammt.akacommand.aliases.special;

import org.bukkit.entity.Player;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

import masecla.mlib.main.MLib;
import teammt.akacommand.aliases.AliasManager;

public class ChangeServerHandler extends SpecialHandler {

    public ChangeServerHandler(MLib lib, AliasManager aliasManager) {
        super(lib, aliasManager);
    }

    @Override
    public String getPrefix() {
        return "%change-server%";
    }

    @Override
    public void handle(Player player, String command) {
        ByteArrayDataOutput output = ByteStreams.newDataOutput();
        output.writeUTF("Connect");
        output.writeUTF(command);

        player.sendPluginMessage(lib.getPlugin(), "BungeeCord", output.toByteArray());
    }

}
