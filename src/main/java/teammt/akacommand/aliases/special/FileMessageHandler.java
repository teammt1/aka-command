package teammt.akacommand.aliases.special;

import org.bukkit.entity.Player;

import masecla.mlib.main.MLib;
import teammt.akacommand.aliases.AliasManager;

public class FileMessageHandler extends SpecialHandler {

    public FileMessageHandler(MLib lib, AliasManager aliasManager) {
        super(lib, aliasManager);
    }

    @Override
    public void handle(Player player, String command) {
        lib.getMessagesAPI().sendMessage(command, player);
    }

    @Override
    public String getPrefix() {
        return "%message-file%";
    }
}
