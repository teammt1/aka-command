package teammt.akacommand.aliases.special;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import masecla.mlib.classes.MamlConfiguration;
import masecla.mlib.classes.messages.text.SendableMessage;
import masecla.mlib.main.MLib;
import teammt.akacommand.aliases.AliasManager;

public class MessageHandler extends SpecialHandler {

    public MessageHandler(MLib lib, AliasManager aliasManager) {
        super(lib, aliasManager);
    }

    @Override
    public String getPrefix() {
        return "%message%";
    }

    @Override
    public void handle(Player player, String command) {
        YamlConfiguration dummyConfig = new YamlConfiguration();
        dummyConfig.set("message", command);
        MamlConfiguration config = new MamlConfiguration(lib, dummyConfig);

        SendableMessage message = new SendableMessage(config, "message", "");

        message.sendToPlayer(player);
    }
}
