package teammt.akacommand.aliases.special;

import org.bukkit.Sound;
import org.bukkit.entity.Player;

import masecla.mlib.main.MLib;
import teammt.akacommand.aliases.AliasManager;

public class SoundHandler extends SpecialHandler {

    public SoundHandler(MLib lib, AliasManager aliasManager) {
        super(lib, aliasManager);
    }

    @Override
    public String getPrefix() {
        return "%sound%";
    }

    @Override
    public void handle(Player player, String command) {
        Sound sound = Sound.valueOf(command);
        player.playSound(player.getLocation(), sound, 1, 1);
    }
}
