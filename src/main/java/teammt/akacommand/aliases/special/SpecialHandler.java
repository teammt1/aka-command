package teammt.akacommand.aliases.special;

import org.bukkit.entity.Player;

import lombok.Getter;
import masecla.mlib.classes.Registerable;
import masecla.mlib.main.MLib;
import teammt.akacommand.aliases.AliasManager;

public abstract class SpecialHandler extends Registerable {

    @Getter
    private AliasManager aliasManager;

    public SpecialHandler(MLib lib, AliasManager aliasManager) {
        super(lib);
        this.aliasManager = aliasManager;
    }

    @Override
    public void register() {
        super.register();
        aliasManager.registerSpecialHandler(this);
    }

    public abstract void handle(Player player, String command);

    public abstract String getPrefix();
}
