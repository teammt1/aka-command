package teammt.akacommand.commands;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import masecla.mlib.annotations.RegisterableInfo;
import masecla.mlib.annotations.SubcommandInfo;
import masecla.mlib.classes.Pair;
import masecla.mlib.classes.Registerable;
import masecla.mlib.classes.Replaceable;
import masecla.mlib.main.MLib;
import teammt.akacommand.aliases.AliasManager;
import teammt.akacommand.aliases.CommandAlias;
import teammt.akacommand.containers.UsageContainer;

@RegisterableInfo(command = "akac")
public class AKACMainCommand extends Registerable {
    private AliasManager aliasManager;

    public AKACMainCommand(MLib lib, AliasManager aliasManager) {
        super(lib);
        this.aliasManager = aliasManager;
    }

    @SubcommandInfo(subcommand = "", permission = "teammt.akac.help")
    public void handleHelpWithoutArguments(CommandSender sender) {
        lib.getMessagesAPI().sendMessage("help-menu", sender);
    }

    @SubcommandInfo(subcommand = "help", permission = "teammt.akac.help")
    public void handleHelpWithArguments(CommandSender sender) {
        this.handleHelpWithoutArguments(sender);
    }

    @SubcommandInfo(subcommand = "reload", permission = "teammt.akac.reload")
    public void handleReload(CommandSender sender) {
        lib.getConfigurationAPI().reloadAll();
        lib.getMessagesAPI().reloadSharedConfig();
        aliasManager.loadAliases();
        lib.getMessagesAPI().sendMessage("plugin-reloaded", sender);
    }

    private Map<UUID, Pair<String, String>> commandAdd = new HashMap<>();

    @SubcommandInfo(subcommand = "add", permission = "teammt.akac.add")
    public void handleAdd(Player player) {
        lib.getMessagesAPI().sendMessage("command-add-initial", player);
        commandAdd.put(player.getUniqueId(), new Pair<>(null, null));
        this.aliasManager.getExcluded().add(player.getUniqueId());
    }

    @EventHandler
    public void onPlayerCancel(AsyncPlayerChatEvent event) {
        if (!commandAdd.containsKey(event.getPlayer().getUniqueId()))
            return;
        event.setCancelled(true);

        if (event.getMessage().equalsIgnoreCase("cancel")) {
            commandAdd.remove(event.getPlayer().getUniqueId());
            this.aliasManager.getExcluded().remove(event.getPlayer().getUniqueId());
            lib.getMessagesAPI().sendMessage("command-add-cancelled", event.getPlayer());
            return;
        }
    }

    @EventHandler
    public void onPlayerChat(PlayerCommandPreprocessEvent event) {
        if (!commandAdd.containsKey(event.getPlayer().getUniqueId()))
            return;
        event.setCancelled(true);

        Pair<String, String> pair = commandAdd.get(event.getPlayer().getUniqueId());
        // pair is key value
        if (pair.getKey() == null) {
            pair.setKey(event.getMessage());
            lib.getMessagesAPI().sendMessage("command-add-alias", event.getPlayer());
        } else if (pair.getValue() == null) {
            pair.setValue(event.getMessage());
            lib.getMessagesAPI().sendMessage("command-add-confirm", event.getPlayer(),
                    new Replaceable("%command%", pair.getKey()),
                    new Replaceable("%alias%", pair.getValue()));
        } else {
            String buttonClicked = event.getMessage();
            commandAdd.remove(event.getPlayer().getUniqueId());

            if (buttonClicked.contains("/akac add confirm")) {
                // Handle adding alias
                String command = pair.getKey();
                String alias = pair.getValue();

                String possibleName = this.aliasManager.getPossibleNameForAlias(alias);
                if (possibleName == null) {
                    lib.getMessagesAPI().sendMessage("command-add-already-exists", event.getPlayer());
                    return;
                }

                List<String> commands = new ArrayList<>();
                commands.add(command);

                CommandAlias commandAlias = new CommandAlias(commands, alias);

                this.aliasManager.createAlias(possibleName, commandAlias);
                lib.getMessagesAPI().sendMessage("command-add-success", event.getPlayer());
            } else if (buttonClicked.contains("/akac add cancel")) {
                lib.getMessagesAPI().sendMessage("command-add-cancelled", event.getPlayer());
            }
        }
    }

    @SubcommandInfo(subcommand = "list", permission = "teammt.akac.list")
    public void handleList(CommandSender sender) {
        for (String aliasId : this.aliasManager.getAliasNames()) {
            CommandAlias alias = this.aliasManager.getAlias(aliasId);
            lib.getMessagesAPI().sendMessage("command-list-format", sender,
                    new Replaceable("%alias%", alias.getAlias()),
                    new Replaceable("%command%", alias.getCommands().get(0)),
                    new Replaceable("%id%", aliasId));
        }
    }

    @SubcommandInfo(subcommand = "remove", permission = "teammt.akac.remove")
    public void handleRemove(CommandSender sender, String alias) {
        if (!this.aliasManager.aliasExists(alias)) {
            lib.getMessagesAPI().sendMessage("command-remove-does-not-exist", sender);
            return;
        }

        this.aliasManager.deleteAlias(alias);
        lib.getMessagesAPI().sendMessage("command-remove-confirm", sender);
    }

    @SubcommandInfo(subcommand = "test", permission = "teammt.akac.test")
    public void handleTest(Player player) {
        Set<UUID> playersInTestMode = this.aliasManager.getInTestMode();
        if (playersInTestMode.contains(player.getUniqueId())) {
            playersInTestMode.remove(player.getUniqueId());
            lib.getMessagesAPI().sendMessage("command-test-mode-exited", player);
        } else {
            playersInTestMode.add(player.getUniqueId());
            lib.getMessagesAPI().sendMessage("command-test-mode-entered", player);
        }
    }

    @SubcommandInfo(subcommand = "usage", permission = "teammt.akac.usage")
    public void handleUsage(Player player) {
        lib.getContainerAPI().openFor(player, UsageContainer.class);
    }
}
