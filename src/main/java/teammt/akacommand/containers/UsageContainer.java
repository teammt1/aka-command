package teammt.akacommand.containers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import masecla.mlib.classes.Pair;
import masecla.mlib.classes.builders.ItemBuilder;
import masecla.mlib.containers.instances.SquaredPagedContainer;
import masecla.mlib.main.MLib;
import teammt.akacommand.usage.UsageManager;

public class UsageContainer extends SquaredPagedContainer {

    private UsageManager usageManager;

    public UsageContainer(MLib lib, UsageManager usageManager) {
        super(lib);
        this.usageManager = usageManager;
    }

    @Override
    public List<ItemStack> getOrderableItems(Player player) {
        Map<String, Map<String, Integer>> basecommandUsages = usageManager.getBasecommandUsages();
        List<ItemStack> items = new ArrayList<>();
        for (String basecommand : basecommandUsages.keySet()) {
            Map<String, Integer> commandUsages = basecommandUsages.get(basecommand);
            List<Pair<String, Integer>> sortedUsages = new ArrayList<>();
            for (String command : commandUsages.keySet()) {
                int usage = commandUsages.get(command);
                sortedUsages.add(new Pair<String, Integer>(command, usage));
            }

            sortedUsages.sort((a, b) -> {
                return b.getValue() - a.getValue();
            });

            items.add(new ItemBuilder(Material.PAPER)
                    .mnl("usage-item").replaceable("%command%", basecommand)
                    .replaceable("%usages%", "&7" + String.join("\n&7", sortedUsages.stream().map((a) -> {
                        return a.getKey() + " (" + a.getValue() + ")";
                    }).toArray(String[]::new))).build(lib, player));
        }

        return items;
    }

    @Override
    public void usableClick(InventoryClickEvent event) {
        event.setCancelled(true);
    }

    @Override
    public int getSize(Player player) {
        return 36;
    }

    @Override
    public int getUpdatingInterval() {
        return 10;
    }

    @Override
    public boolean requiresUpdating() {
        return true;
    }

    @Override
    public String getTitle(Player player) {
        return lib.getMessagesAPI().getPluginMessage("usage-title", player);
    }

}
