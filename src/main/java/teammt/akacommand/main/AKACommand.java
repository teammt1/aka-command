package teammt.akacommand.main;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import masecla.mlib.main.MLib;
import teammt.akacommand.aliases.AliasManager;
import teammt.akacommand.aliases.CommandRegisterer;
import teammt.akacommand.commands.AKACMainCommand;
import teammt.akacommand.containers.UsageContainer;
import teammt.akacommand.usage.UsageManager;

public class AKACommand extends JavaPlugin {

	private MLib lib;
	private AliasManager aliasManager;
	private UsageManager usageManager;
	private CommandRegisterer commandRegisterer;

	private void setupAntispamDelay() {
		lib.getMessagesAPI().setAntispamDelay("command-list-format", 0);
		lib.getMessagesAPI().setAntispamDelay("command-remove-confirm", 0);
	}

	@Override
	public void onEnable() {
		this.lib = new MLib(this);
		lib.getConfigurationAPI().requireAll();
		this.setupAntispamDelay();

		this.openBungeecordChannel();

		this.usageManager = new UsageManager(lib);
		this.usageManager.register();
		this.aliasManager = new AliasManager(lib, usageManager);
		this.aliasManager.register();

		// Commands
		new AKACMainCommand(lib, aliasManager).register();

		// Containers
		new UsageContainer(lib, usageManager).register();

		this.commandRegisterer = new CommandRegisterer(lib, this, aliasManager);
		this.commandRegisterer.register();
	}

	@Override
	public void onDisable() {
		lib.getConfigurationAPI().updateFile("data");
	}

	private void openBungeecordChannel() {
		Bukkit.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
	}
}
