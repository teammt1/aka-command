package teammt.akacommand.usage;

import java.util.HashMap;
import java.util.Map;

import lombok.Getter;
import masecla.mlib.classes.Registerable;
import masecla.mlib.main.MLib;

public class UsageManager extends Registerable {

    @Getter
    private Map<String, Integer> usages = new HashMap<>();

    public UsageManager(MLib lib) {
        super(lib);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void register() {
        super.register();

        usages = (Map<String, Integer>) lib.getConfigurationAPI().getConfig("data")
                .get("usages", new HashMap<>());
    }

    public void saveToFile() {
        lib.getConfigurationAPI().getConfig("data").set("usages", usages);
        lib.getConfigurationAPI().updateFile("data");
    }

    public void ingestCommand(String command) {
        usages.merge(command, 1, (a, b) -> a + b);
        saveToFile();
    }

    public Map<String, Map<String, Integer>> getBasecommandUsages() {
        Map<String, Map<String, Integer>> basecommandUsages = new HashMap<>();
        for (String command : usages.keySet()) {
            String basecommand = command.split(" ")[0];
            if (!basecommandUsages.containsKey(basecommand))
                basecommandUsages.put(basecommand, new HashMap<>());
            basecommandUsages.get(basecommand).put(command, usages.get(command));
        }
        return basecommandUsages;
    }
}
