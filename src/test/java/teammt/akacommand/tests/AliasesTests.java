package teammt.akacommand.tests;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import teammt.akacommand.aliases.CommandAlias;
import teammt.testingagent.annotations.Test;
import static teammt.testingagent.testing.Assertions.*;

public class AliasesTests {
    @Test
    public void testSimpleAlias() {
        CommandAlias alias = new CommandAlias(Arrays.asList("/give hello"), "/i");

        PlayerCommandPreprocessEvent event = mock(PlayerCommandPreprocessEvent.class);
        when(event.getMessage()).thenReturn("/i");

        assertEquals(Arrays.asList("/give hello"), alias.matches(event), "The alias should match the command");
    }

    @Test
    public void testAliasWithArguments() {
        CommandAlias alias = new CommandAlias(Arrays.asList("/give hello <item>"), "/i <item>");

        PlayerCommandPreprocessEvent event = mock(PlayerCommandPreprocessEvent.class);
        when(event.getMessage()).thenReturn("/i diamond");

        assertEquals(Arrays.asList("/give hello diamond"), alias.matches(event), "The alias should match the command");
    }

    @Test
    public void handleAliasMultipleArgumentsOutOfOrder() {
        CommandAlias alias = new CommandAlias(Arrays.asList("/give hello <item> <amount>"), "/i <amount> <item>");

        PlayerCommandPreprocessEvent event = mock(PlayerCommandPreprocessEvent.class);
        when(event.getMessage()).thenReturn("/i 64 diamond");

        assertEquals(Arrays.asList("/give hello diamond 64"), alias.matches(event),
                "The alias should match the command");
    }

    @Test
    public void handleAliasTwiceNotBeingChanged() {
        CommandAlias alias = new CommandAlias(Arrays.asList("/give hello <item> <amount>"),
                "/i <amount> <item>");

        PlayerCommandPreprocessEvent event = mock(PlayerCommandPreprocessEvent.class);
        when(event.getMessage()).thenReturn("/i 64 diamond");
        assertEquals(Arrays.asList("/give hello diamond 64"), alias.matches(event),
                "The alias should match the command");

        PlayerCommandPreprocessEvent event2 = mock(PlayerCommandPreprocessEvent.class);
        when(event2.getMessage()).thenReturn("/i 32 dirt");
        assertEquals(Arrays.asList("/give hello dirt 32"), alias.matches(event2),
                "The alias should match the command");
    }
}
